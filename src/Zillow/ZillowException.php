<?php

namespace Drupal\zillow_api\Zillow;

/**
 * Default exception class.
 */
class ZillowException extends \Exception {
}
