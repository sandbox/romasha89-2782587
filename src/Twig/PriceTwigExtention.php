<?php
/**
 * @file
 * Contains \Drupal\zillow_api\Twig\PriceTwigExtention
 */
namespace Drupal\zillow_api\Twig;

/**
 * Class PriceTwigExtention.
 *
 * @package Drupal\zillow_api\Twig
 */
class PriceTwigExtention extends \Twig_Extension {

  /**
   * Filter call.
   *
   * @return array|\Twig_SimpleFilter[]
   */
  public function getFilters() {
    return array(
      new \Twig_SimpleFilter('price', array($this, 'priceFilter')),
    );
  }

  /**
   * Price array to string conversion in TWIG.
   *
   * @param int|mixed $number
   *    Input data.
   *
   * @return string
   *    Output string.
   */
  public function priceFilter($number) {
    if (isset($number['#'])) {
      $price = number_format((double) $number['#'], 2);
      $price .= ' ' . $number['@currency'];
    }
    else {
      $price = number_format((double) $number, 2);
    }

    return $price;
  }

  /**
   * Get filter name.
   *
   * @return string
   *    Filter name.
   */
  public function getName() {
    return 'zillow_api.twig_extension';
  }

}
