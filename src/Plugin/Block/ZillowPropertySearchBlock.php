<?php
/**
 * @file
 * Contains \Drupal\zillow_api\Plugin\Block\ZillowPropertySearchBlock.
 */
namespace Drupal\zillow_api\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * Provides Zillow Property Search block.
 *
 * @Block(
 *   id = "zillow_property_search_block",
 *   admin_label = @Translation("Zillow Property Search"),
 *   category = @Translation("Blocks")
 * )
 */
class ZillowPropertySearchBlock extends BlockBase implements ContainerFactoryPluginInterface {
  /**
   * The form builder.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, FormBuilderInterface $formBuilder) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->formBuilder = $formBuilder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('form_builder')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    // Get Block form.
    $builtForm = $this->formBuilder->getForm('Drupal\zillow_api\Plugin\Form\ZillowPropertySearchBlockForm');
    $render['form'] = $builtForm;

    return $render;
  }

}
