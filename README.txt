# A help file for Zillow

This is Zillow API demonstration module for Drupal 8.
Drupal compatibility: 8.1.x and greater.

Useful links:

PHP Zillow - https://github.com/VinceG/zillow
Google Geocoder, Google Placemark - https://github.com/robap/zillgoog/tree/master/app/lib
GeoComplete - http://ubilabs.github.io/geocomplete/

Module Features:
    Zillow API's Support:
        - GetDeepSearchResults;
        - GetChart;
        - GetDeepComps;
        - GetRegionChildren.
    Featured Blocks:
        - Property Search form;
        - Region Search form.
    Pages:
        - Property Search Results:
            * Property Details;
            * Price chart;
            * Comparables table with details;
            * Google Map with markers and StreetView feature(Click on marker).
        - Region Search Results:
            * Neighborhood subregions;
            * City subregions;
            * zipcode subregions;
            * Google Maps preview(static image) for each region in results table;
            * Provided link to zillow.com for each region in results table.
        - Zillow module settings page (Admin > Configuration > Zillow Settings):
            * Zillow API key;
            * Google API key;
            * Results per page on Region Search Results Page;
            * Chart Period;
            * Comparables Number.

To Get Zillow API Key follow link on registration page: http://www.zillow.com/webservice/Registration.htm